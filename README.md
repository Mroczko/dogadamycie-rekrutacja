# Zadanie rekrutacyjne - Andrzej Mroczko

## Zadanie
- W bazie chcemy przechowywać kategorie w 4 wersjach językowych (PL / EN / DE / FR)
- Napisanie migracji do bazy
- Napisanie seeder’a wypełniającego bazę kilkoma testowymi kategoriami
- Endpoint umożliwiający pobranie listy kategorii dla wybranego locale
- Endpoint umożliwiający dodanie nowej kategorii (definicja nazwy kategorii dla wymaganych locale)
- Napisanie listenera, który w momencie dodania nowej kategorii wyślę notyfikację mailową pod 
  zdefiniowany adres. *wysyłka notyfikacji nie musi być realizowana, pusta metoda notyfikacji w odpowiednim miejscu.
- Test: przykładowy test sprawdzający poprawność działania powyższych endpointów.

## Instalacja

Wymagania:
- php 8.1

Laravel jako to laravel:

- Skopiuj .env.example do .env 
- Stwórz bazę danych i uzupełnij enva
- W konsoli odpal `composer install`
- Jeśli klucz nie ustawił się automatycznie ustaw go manualnie przez `php artisan key:generate`
- Puść migrację z seederem `php artisan migrate --seed`
- Gotowe



## Opis braku realizacji

Brak realizacji :( 

Do pisania testów nawet nie dotarłem, bo ogólnie mój pomysł na to zadanie padł.   

Po pierwsze - pomyliłem frameworki, i18n nie jest out of the box w laravelu tylko w cake'u, 
więc jedno uproszczenie odpadło. 

Po drugie, uparłem się żeby zrobić to w podobny sposób, więc chciałem zrobić osobne 
tabele z tłumaczeniami dla każdego modelu, ale bez użycia modeli pod tłumaczenia, czy 
tam z pominięciem jednej globalnej tabeli 'translates'. 

Po trzecie - left join na Laravelu nie grupuje wyników, a tworzenie dynamicznych relacji 
w oparciu o dynamiczne modele zdaje się że nie ma sensu (i chyba to niezbyt możliwe do 
zrobienia xD), tak więc albo bym wszystko musiał zaczynać od nowa, albo robić jakąś kompletnie
niewydajną bzdurę typu pobieranie translacji przy geterze z zapytaniem do bazy danych przy 
każdym odpytaniu. 

No nic, czas jaki sobie na to dałem przekroczyłem dość mocno, bo już 5h się bawię, a coraz wiecej 
błędów w tym rozwiązaniu. 

Z pomysłów jeszcze jakie miałęm na rozwiązanie tematu translacji, to oczywiście w oparciu o model 
i relację do niego, lub np. przechowanie tłumaczeń w jsonie.

Jednak ostatecznie, wydaje mi się że oba rozwiazania są niewydajne i odporne na "przeszukiwanie". 
Jestem ciekawy jakie rozwiązanie spodziewaliście sie znaleźć / jakie było by odpowiednie.
