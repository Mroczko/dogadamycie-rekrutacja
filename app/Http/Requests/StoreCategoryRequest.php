<?php

namespace App\Http\Requests;

class StoreCategoryRequest extends ApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'array:'.join(',', config('i18n.languages'))],
            'name.*' => ['string']
        ];
    }

    public function messages()
    {
        return [
            'name.array' => 'atribute `name` must be an array with folowing keys: ['.join(',', config('i18n.languages')).'] only'
        ];
    }

}
