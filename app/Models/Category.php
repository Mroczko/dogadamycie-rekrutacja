<?php

namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends TranslateModel
{
    use SoftDeletes;

    protected $fillable = ['name'];

    protected $toTranslate = ['name'];

}
