<?php

namespace App\Models;

use App\Services\I18n\Concerns\TranslateFields;
use Illuminate\Database\Eloquent\Model;

abstract class TranslateModel extends Model
{
    use TranslateFields;
}
