<?php

namespace App\Services\I18n;

class LanguagesHelper
{
    public static function get(): array
    {
        return config('i18n.languages',[]);
    }
}
