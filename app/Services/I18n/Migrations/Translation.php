<?php

namespace App\Services\I18n\Migrations;

use App\Services\I18n\TranslationTableHelper;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Translation
{
    public static function create(string $table, string $primaryKeyField = 'id'): void
    {
        $translationTableName = TranslationTableHelper::getTranslationTableName($table);
        Schema::create($translationTableName, function (Blueprint $translationTable) use ($table, $primaryKeyField) {
            $foreignId = TranslationTableHelper::getForeignKey($table, $primaryKeyField);
            $translationTable->foreignId($foreignId);
            $translationTable->string('field');
            $translationTable->string('lang',8);
            $translationTable->string('value');
            $translationTable->primary([$foreignId,'field','lang']);
            $translationTable->foreign($foreignId)
                ->references($primaryKeyField)
                ->on($table)
                ->onDelete('cascade');
        });
    }

    public static function destroy(string $table): void
    {
        Schema::dropIfExists(TranslationTableHelper::getTranslationTableName($table));
    }
}
