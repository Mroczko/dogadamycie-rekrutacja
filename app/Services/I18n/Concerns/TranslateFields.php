<?php

namespace App\Services\I18n\Concerns;

use App\Services\I18n\TranslationTableHelper;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

trait TranslateFields
{
    protected $toTranslate = [];

    /**
     * Boot the soft deleting trait for a model.
     *
     * @return void
     */
    public static function bootTranslateFields()
    {
        static::addGlobalScope('translate', function (Builder $builder) {
            $table = $builder->getModel()->getTable();
            $primaryKey = $builder->getModel()->getKeyName();

            $builder->leftJoin(TranslationTableHelper::getTranslationTableName($table), fn($join) => $join
                ->on($primaryKey, '=', TranslationTableHelper::getForeignKey($table, $primaryKey))
                ->where('lang','=',Lang::getLocale())
            );
        });
    }

    public function getAttribute($key) {
        return parent::getAttribute($key);
    }

    public function addTranslations(string $field, array $values): void
    {
        foreach ($values as $lang => $value) {
            $this->addTranslation($field, $value, $lang);
        }
    }

    public function addTranslation(string $field, string $value, ?string $lang = null): void
    {
        if(!in_array($field, $this->toTranslate)) {
            return;
        }

        DB::table(TranslationTableHelper::getTranslationTableName($this->table))
            ->updateOrInsert([
                TranslationTableHelper::getForeignKey($this->table) => $this->getKey(),
                'field' => $field,
                'lang' => $lang??Lang::getLocale(),
            ],[
                'value' => $value
            ]);
    }

}
