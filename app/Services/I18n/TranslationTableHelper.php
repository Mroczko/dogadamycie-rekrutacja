<?php

namespace App\Services\I18n;

use Illuminate\Support\Str;

class TranslationTableHelper
{
    public static function getTranslationTableName(string $tableName): string
    {
        return $tableName.'_'.config('i18n.table_suffix');
    }

    public static function getForeignKey(string $tableName, string $primaryKey = 'id'): string
    {
        return Str::singular($tableName).'_'.$primaryKey;
    }
}
