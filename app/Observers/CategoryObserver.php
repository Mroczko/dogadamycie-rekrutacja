<?php

namespace App\Observers;

use App\Mail\CategoryCreated;
use App\Models\Category;
use Illuminate\Support\Facades\Log;

class CategoryObserver
{

    public $afterCommit = true;

    public function created(Category $category): void
    {
        Log::info('new category created');
        // Mail::to(config('notifications.to'))->send(new CategoryCreated($category));
    }
}
