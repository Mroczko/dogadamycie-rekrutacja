<?php

return [
    'languages' => [
        'pl','en','de','fr'
    ],
    'table_suffix' => 'i18n',
];
